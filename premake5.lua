-- premake5.lua
workspace "json"
    configurations { "Debug", "Release" }
    platforms { "Win32" }
	
project "json"
	kind "StaticLib"
	language "C++"
	targetdir "%{cfg.buildcfg}"
	characterset "MBCS"
	
    files { "json.h", "json.cpp" }
	
	filter "configurations:Debug"
        defines { "DEBUG" }
        flags { "Symbols" }

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

project "tests"
	kind "ConsoleApp"
	language "C++"
	targetdir "%{cfg.buildcfg}"
	characterset "MBCS"
	
	includedirs { "%{solution().basedir}", "tests", "external" }
    links { "json" }
    files { "tests/**.h", "tests/**.cpp" }
	
	filter "configurations:Debug"
        defines { "DEBUG" }
        flags { "Symbols" }

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"