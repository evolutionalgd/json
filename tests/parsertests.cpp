
#include "upptest/upptest.h"
#include "json.h"
#include "testfixture.h"

#define IMPLEMENT_UNITTEST_FOR_PARSER(name)	TEST_F(name, ValueTestFixture, "json.parser")


IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Empty_ReturnsNull)
{
	json::Parser parser;
	auto result = parser.parse("");
	UASSERT_NOT_NULL(result.get());
	UASSERT_TRUE(result->is_null());
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_null_ReturnsNull)
{
	json::Parser parser;
	auto result = parser.parse("null");
	UASSERT_NOT_NULL(result.get());
	UASSERT_TRUE(result->is_type(json::ValueType::Null));
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_true_ReturnsBoolAndIsTrue)
{
	json::Parser parser;
	auto result = parser.parse("true");
	AssertValue(result, true);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_false_ReturnsBoolAndIsFalse)
{
	json::Parser parser;
	auto result = parser.parse("false");
	AssertValue(result, false);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_String_SingleQuotedEmptyString)
{
	json::Parser parser;
	auto result = parser.parse("''");
	AssertValue(result, "");
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_String_SingleQuotedString)
{
	json::Parser parser;
	auto result = parser.parse("'hello'");
	AssertValue(result, "hello");
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_String_DoubleQuotedEmptyString)
{
	json::Parser parser;
	auto result = parser.parse("\"\"");
	AssertValue(result, "");
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Int_Positive)
{
	json::Parser parser;
	auto result = parser.parse("123");
	AssertValue(result, 123);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Int_Negative)
{
	json::Parser parser;
	auto result = parser.parse("-123");
	AssertValue(result, -123);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Float_Positive)
{
	json::Parser parser;
	auto result = parser.parse("123.123");
	AssertValue(result, 123.123f);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Float_Negative)
{
	json::Parser parser;
	auto result = parser.parse("-123.123");
	AssertValue(result, -123.123f);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Float_NoInteger)
{
	json::Parser parser;
	auto result = parser.parse(".123");
	AssertValue(result, 0.123f);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_Float_NoIntegerNegative)
{
	json::Parser parser;
	auto result = parser.parse("-.123");
	AssertValue(result, -0.123f);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_String_DoubleQuotedString)
{
	json::Parser parser;
	auto result = parser.parse("\"hello\"");
	AssertValue(result, "hello");
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_EmptySquareBraces_ReturnsArray)
{
	json::Parser parser;
	auto result = parser.parse("[]");
	AssertValue(result, json::ArrayType());
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_EmptyBraces_ReturnsObject)
{
	json::Parser parser;
	auto result = parser.parse("{}");
	AssertValue(result, json::ObjectType());
}
	
IMPLEMENT_UNITTEST_FOR_PARSER(Parse_ObjectOneProperty_ReturnsObject)
{
	json::ObjectType expected;
	expected["test"] = std::make_shared<json::ObjectValue>(json::ObjectType());

	json::Parser parser;
	auto result = parser.parse("{'test':{}}");
	AssertValue(result, expected);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_ObjectTwoProperty_ReturnsObject)
{
	json::ObjectType expected;
	expected["test"] = std::make_shared<json::ObjectValue>(json::ObjectType());
	expected["other"] = std::make_shared<json::ArrayValue>(json::ArrayType());

	json::Parser parser;
	auto result = parser.parse("{'test':{},  'other':[]}");
	AssertValue(result, expected);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_ArraySingleObject)
{
	json::ArrayType expected;
	expected.push_back(std::make_shared<json::ObjectValue>(json::ObjectType()));

	json::Parser parser;
	auto result = parser.parse("[ {} ]");
	AssertValue(result, expected);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_ArrayTwoObjects)
{
	json::ArrayType expected;
	expected.push_back(std::make_shared<json::ObjectValue>(json::ObjectType()));
	expected.push_back(std::make_shared<json::ObjectValue>(json::ObjectType()));

	json::Parser parser;
	auto result = parser.parse("[ {},  {} ]");
	AssertValue(result, expected);
}

IMPLEMENT_UNITTEST_FOR_PARSER(Parse_ArrayInts)
{
	json::ArrayType expected;
	expected.push_back(std::make_shared<json::ObjectValue>(json::ObjectType()));

	json::Parser parser;
	auto result = parser.parse("[ {} ]");
	AssertValue(result, expected);
}

#undef IMPLEMENT_UNITTEST_FOR_PARSER
