#include "upptest/upptest.h"
#include "json.h"
#include "testfixture.h"

class WriterTestFixture 
	: public ValueTestFixture
{
protected:
	std::string write(const json::ValuePtr& value)
	{
		json::Writer writer;
		std::ostringstream ss;
		writer.write(value, ss);
		return  ss.str();
	}
};

#define IMPLEMENT_UNITTEST_FOR_WRITER(name)	TEST_F(name, WriterTestFixture, "json.writer")


IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_EmptyObject)
{
	auto value = std::make_shared<json::ObjectValue>();
	auto str = write(value);
	UASSERT_EQ(std::string("{}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithEmptyArrayProperty)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::ArrayValue>()));
	auto value = std::make_shared<json::ObjectValue>(obj);			

	auto str = write(value);

	UASSERT_EQ(std::string("{\"test\": []}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithIntProperty)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::IntValue>(123)));
	auto value = std::make_shared<json::ObjectValue>(obj);

	auto str = write(value);

	UASSERT_EQ(std::string("{\"test\": 123}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithFloatProperty)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::FloatValue>(123.123f)));
	auto value = std::make_shared<json::ObjectValue>(obj);

	auto str = write(value);

	UASSERT_EQ(std::string("{\"test\": 123.123}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithBoolProperty)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::BoolValue>(true)));
	auto value = std::make_shared<json::ObjectValue>(obj);

	auto str = write(value);

	UASSERT_EQ(std::string("{\"test\": true}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithNullProperty)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::NullValue>()));
	auto value = std::make_shared<json::ObjectValue>(obj);

	auto str = write(value);

	UASSERT_EQ(std::string("{\"test\": null}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithStringProperty)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::StringValue>("value")));
	auto value = std::make_shared<json::ObjectValue>(obj);

	auto str = write(value);

	UASSERT_EQ(std::string("{\"test\": \"value\"}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ObjectWithTwoProperties)
{
	json::ObjectType obj;
	obj.insert(std::make_pair(std::string("test"), std::make_shared<json::StringValue>("value")));
	obj.insert(std::make_pair(std::string("other"), std::make_shared<json::IntValue>(0)));
	auto value = std::make_shared<json::ObjectValue>(obj);

	auto str = write(value);

	UASSERT_EQ(std::string("{\"other\": 0, \"test\": \"value\"}"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_EmptyArray)
{
	auto value = std::make_shared<json::ArrayValue>();
	auto str = write(value);
	UASSERT_EQ(std::string("[]"), str);
}

IMPLEMENT_UNITTEST_FOR_WRITER(Writer_Write_ArrayOfInts)
{
	json::ArrayType arr;
	arr.push_back(std::make_shared<json::IntValue>(123));
	arr.push_back(std::make_shared<json::IntValue>(456));
	auto value = std::make_shared<json::ArrayValue>(arr);
	auto str = write(value);
	UASSERT_EQ(std::string("[123, 456]"), str);
}

#undef IMPLEMENT_UNITTEST_FOR_WRITER