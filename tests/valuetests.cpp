#include "upptest/upptest.h"
#include "json.h"
#include "testfixture.h"
	
#define IMPLEMENT_UNITTEST_FOR_VALUE(name)	TEST(name, "json.value")


IMPLEMENT_UNITTEST_FOR_VALUE(String_TypeName_IsString)
{
	json::StringValue json_value("test");
	UASSERT_EQ(std::string("string"), json_value.type_name());
}
		
IMPLEMENT_UNITTEST_FOR_VALUE(String_Get_ReturnsTrueAndValue)
{
	json::StringValue json_value("a string");
	std::string result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(std::string("a string"), result);
}


//--------------------------------------------------------------


IMPLEMENT_UNITTEST_FOR_VALUE(Int_TypeName_IsInteger)
{
	json::IntValue json_value;
	UASSERT_EQ(std::string("integer"), json_value.type_name());
}

IMPLEMENT_UNITTEST_FOR_VALUE(Int_Get_WithInt_GetsValue)
{
	json::IntValue json_value(123);
	int result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(123, result);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Int_Get_WithFloat_GetsValue)
{
	json::IntValue json_value(123);
	float result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_FEQ(123.0f, result, 0.001f);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Int_Get_WithBool_GetsTrue)
{
	json::IntValue json_value(123);
	bool result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(true, result);
}

//--------------------------------------------------------------


IMPLEMENT_UNITTEST_FOR_VALUE(Float_TypeName_IsFloat)
{
	json::FloatValue json_value;
	UASSERT_EQ(std::string("float"), json_value.type_name());
}

IMPLEMENT_UNITTEST_FOR_VALUE(Float_Get_WithFloat_GetsValue)
{
	json::FloatValue json_value(123.123f);
	float result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_FEQ(123.123f, result, 0.001f);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Float_Get_WithInt_GetsValue)
{
	json::FloatValue json_value(123.123f);
	int result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(123, result);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Float_Get_WithBool_GetsTrue)
{
	json::FloatValue json_value(123.123f);
	bool result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(true, result);
}


//--------------------------------------------------------------

IMPLEMENT_UNITTEST_FOR_VALUE(Bool_TypeName_IsBoolean)
{
	json::BoolValue json_value;
	UASSERT_EQ(std::string("boolean"), json_value.type_name());
}

IMPLEMENT_UNITTEST_FOR_VALUE(Bool_Get_WithBool_GetsTrue)
{
	json::BoolValue json_value(true);
	bool result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(true, result);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Bool_Get_WithInt_GetsOne)
{
	json::BoolValue json_value(true);
	int result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(1, result);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Bool_Get_WithFloat_GetsOne)
{
	json::BoolValue json_value(true);
	float result;

	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(1.0f, result);
}


//--------------------------------------------------------------

IMPLEMENT_UNITTEST_FOR_VALUE(Null_TypeName_IsNull)
{
	json::NullValue json_value;
	UASSERT_EQ(std::string("null"), json_value.type_name());
}

IMPLEMENT_UNITTEST_FOR_VALUE(Null_Get_WithInt_Gets0)
{
	json::NullValue json_value;
	int result;
	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(0, result);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Null_Get_WithFloat_Gets0)
{
	json::NullValue json_value;
	float result;
	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(0.0f, result);
}

IMPLEMENT_UNITTEST_FOR_VALUE(Null_Get_WithBool_GetsFalse)
{
	json::NullValue json_value;
	bool result;
	UASSERT_TRUE(json_value.get(result));
	UASSERT_EQ(false, result);
}


//--------------------------------------------------------------

IMPLEMENT_UNITTEST_FOR_VALUE(Array_TypeName_IsArray)
{
	json::ArrayValue json_value;
	UASSERT_EQ(std::string("array"), json_value.type_name());
}

//--------------------------------------------------------------


IMPLEMENT_UNITTEST_FOR_VALUE(Object_TypeName_IsObject)
{
	json::ObjectValue json_value;
	UASSERT_EQ(std::string("object"), json_value.type_name());
}

#undef IMPLEMENT_UNITTEST_FOR_VALUE