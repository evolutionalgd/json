#define UTEST_CPP_IMPLEMENTATION
#include "upptest/upptest.h"
#include <iostream>

int main(int /*argc*/, const char** /*argv*/)
{
	auto res = utest::runner::run_registered([](const utest::result& tst) {
		std::cout << "'" << tst.info->name << "' executed in "
			<< tst.duration.count() << "ms with result: ";
		if (tst.status == utest::status::pass)
			std::cout << "pass" << std::endl;
		else
			std::cout << "failed" << std::endl;
	});

	return res == utest::status::pass ? 0 : -1;
}
