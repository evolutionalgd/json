#pragma once

#include "upptest/upptest.h"
#include "json.h"

inline std::ostream& operator << (std::ostream& os, const json::ValueType& v)
{
	os << static_cast<std::underlying_type<json::ValueType>::type>(v);
	return os;
}

#define UASSERT_FEQ(expected, actual, epsilon)	UASSERT( fabs(expected - actual) < epsilon )

TEST_FIXTURE(ValueTestFixture)
{
protected:
	ValueTestFixture() {}

	template<typename T>
	void AssertValue(const json::ValuePtr& value, const T& expected)
	{
		UASSERT_FAIL("Not mapped");
	}

	template<typename T>
	void AssertValue(const json::ValuePtr& value, T* expected)
	{
		UASSERT_FAIL("Not mapped");
	}

	template<>
	void AssertValue<bool>(const json::ValuePtr& value, const bool& expected)
	{
		UASSERT_NOT_NULL(value.get());
		UASSERT_EQ(expected, value->to_bool());
	}

	template<>
	void AssertValue<int>(const json::ValuePtr& value, const int& expected)
	{
		UASSERT_NOT_NULL(value.get());
		UASSERT_EQ(expected, value->to_int());
	}

	template<>
	void AssertValue<float>(const json::ValuePtr& value, const float& expected)
	{
		UASSERT_NOT_NULL(value.get());
		UASSERT_FEQ(expected, value->to_float(), 0.001f);
	}

	template<>
	void AssertValue<std::string>(const json::ValuePtr& value, const std::string& expected)
	{
		UASSERT_NOT_NULL(value.get());
		UASSERT_EQ(expected, value->to_string());
	}

	template<>
	void AssertValue<json::ArrayType>(const json::ValuePtr& value, json::ArrayType* expected)
	{
		UASSERT_NOT_NULL(expected);
		AssertValue(value, *expected);
	}

	template<>
	void AssertValue<json::ArrayType>(const json::ValuePtr& value, const json::ArrayType& expected)
	{
		UASSERT_NOT_NULL(value.get());
		UASSERT_TRUE(value->is_type(json::ValueType::Array));
		std::vector<json::ValuePtr> *values;
		UASSERT_TRUE(value->get(values));
		UASSERT_EQ(expected.size(), values->size());
		for (auto i = 0U; i < values->size(); ++i)
		{
			AssertValue(expected[i], values->at(i));
		}
	}

	template<>
	void AssertValue<json::ObjectType>(const json::ValuePtr& value, json::ObjectType* expected)
	{
		UASSERT_NOT_NULL(expected);
		AssertValue(value, *expected);
	}

	template<>
	void AssertValue<json::ObjectType>(const json::ValuePtr& value, const json::ObjectType& expected)
	{
		UASSERT_NOT_NULL(value.get());
		UASSERT_TRUE(value->is_type(json::ValueType::Object));
		json::ObjectType *obj;
		UASSERT_TRUE(value->get(obj));
		UASSERT_EQ(expected.size(), obj->size());
		for (auto it : expected)
		{
			AssertProperty(*obj, it.first, it.second);
		}
	}

	void AssertProperty(const json::ObjectType& obj, const std::string& name, const json::ValuePtr& expected)
	{
		auto it = obj.find(name);
		UASSERT_FALSE(it == obj.end());
		AssertValue((*it).second, expected);
	}

	void AssertValue(const json::ValuePtr& value, const char* expected)
	{
		AssertValue(value, std::string(expected));
	}

	void AssertValue(const json::ValuePtr& value, const json::ValuePtr& expected)
	{
		UASSERT_EQ(expected->type(), value->type());
		switch (value->type())
		{
		case json::ValueType::Integer:
		{
			AssertValue(value, expected->to_int());
			break;
		}
		case json::ValueType::Float:
		{
			AssertValue(value, expected->to_float());
			break;
		}
		case json::ValueType::String:
		{
			AssertValue(value, expected->to_string());
			break;
		}
		case json::ValueType::Null:
		{
			break;
		}
		case json::ValueType::Bool:
		{
			AssertValue(value, expected->to_bool());
			break;
		}
		case json::ValueType::Array:
		{
			AssertValue(value, expected->to_array());
			break;
		}
		case json::ValueType::Object:
		{
			AssertValue(value, expected->to_object());
			break;
		}
		}
	}
};

