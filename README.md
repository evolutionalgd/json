## About ##
Small C++11 Json DOM Parser & Writer.

## Usage ##
See tests for usage.

## TODO ##
 - Unicode support
 - String escaping
 - Add nice formatting to the writer
 - Negative testing (eg: broken json)
 - Improve Efficiency


## License ##

This is free and unencumbered software released into the public domain. See UNLICENSE for details.