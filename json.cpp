#include "json.h"

#include <cctype>

namespace json
{

	Value::Value()
		: Value(ValueType::None)
	{}
	
	Value::Value(const ValueType type)
		: _type(type)
	{}

	float Value::to_float() const
	{
		float value(0);
		if (get(value))
		{
			return value;
		}
		return 0;
	}

	int Value::to_int() const
	{
		int value(0);
		if (get(value))
		{
			return value;
		}
		return 0;
	}

	bool Value::to_bool() const
	{
		bool value(false);
		if (get(value))
		{
			return value;
		}
		return false;
	}

	std::string Value::to_string() const
	{
		std::string value;
		if (get(value))
		{
			return value;
		}
		return "";
	}

	const ArrayType* Value::to_array() const
	{
		const ArrayType* value;
		if (get(value))
		{
			return value;
		}
		return nullptr;
	}

	ArrayType* Value::to_array() 
	{
		ArrayType* value;
		if (get(value))
		{
			return value;
		}
		return nullptr;
	}

	const ObjectType* Value::to_object() const
	{
		const ObjectType* value;
		if (get(value))
		{
			return value;
		}
		return nullptr;
	}

	ObjectType* Value::to_object()
	{
		ObjectType* value;
		if (get(value))
		{
			return value;
		}
		return nullptr;
	}

	//--------------------------------------------------------------

	StringValue::StringValue()
		: StringValue("")
	{}

	StringValue::StringValue(const std::string& value)
		: Value(ValueType::String)
		, _value(value)
	{}

	bool StringValue::get_impl(std::string& out_value) const
	{
		out_value = _value;
		return true;
	}

	//--------------------------------------------------------------

	BoolValue::BoolValue()
		: BoolValue(false)
	{}

	BoolValue::BoolValue(const bool value)
		: Value(ValueType::Bool)
		, _value(value)
	{}

	bool BoolValue::get_impl(float& out_value) const
	{
		out_value = _value ? 1.0f : 0;
		return true;
	}

	bool BoolValue::get_impl(int& out_value) const
	{
		out_value = _value ? 1: 0;
		return true;
	}

	bool BoolValue::get_impl(bool& out_value) const
	{
		out_value = _value;
		return true;
	}
	
	//--------------------------------------------------------------

	IntValue::IntValue()
		: IntValue(0)
	{}

	IntValue::IntValue(const int value)
		: Value(ValueType::Integer)
		, _value(value)
	{}

	bool IntValue::get_impl(float& out_value) const
	{
		out_value = static_cast<float>(_value);
		return true;
	}

	bool IntValue::get_impl(int& out_value) const
	{
		out_value = _value;
		return true;
	}

	bool IntValue::get_impl(bool& out_value) const
	{
		out_value = _value >= 1;
		return true;
	}

	//--------------------------------------------------------------

	FloatValue::FloatValue()
		: FloatValue(0)
	{}

	FloatValue::FloatValue(const float value)
		: Value(ValueType::Float)
		, _value(value)
	{}

	bool FloatValue::get_impl(float& out_value) const
	{
		out_value = _value;
		return true;
	}

	bool FloatValue::get_impl(int& out_value) const
	{
		out_value = static_cast<int>(_value);
		return true;
	}

	bool FloatValue::get_impl(bool& out_value) const
	{
		out_value = _value >= 1.0f;
		return true;
	}

	//--------------------------------------------------------------

	NullValue::NullValue()
		: Value(ValueType::Null)
	{}

	bool NullValue::get_impl(float& out_value) const
	{
		out_value = 0;
		return true;
	}

	bool NullValue::get_impl(int& out_value) const
	{
		out_value = 0;
		return true;
	}

	bool NullValue::get_impl(bool& out_value) const
	{
		out_value = false;
		return true;
	}

	//--------------------------------------------------------------

	ObjectValue::ObjectValue()
		: ObjectValue(ObjectType())
	{}

	ObjectValue::ObjectValue(const ObjectType &value)
		: Value(ValueType::Object)
		, _value(value)
	{}

	ObjectType& ObjectValue::data()
	{
		return _value;
	}

	const ObjectType& ObjectValue::data() const
	{
		return _value;
	}

	bool ObjectValue::get_impl(const ObjectType*& out_value) const
	{
		out_value = &_value;
		return true;
	}

	bool ObjectValue::get_impl(ObjectType*& out_value)
	{
		out_value = &_value;
		return true;
	}

	//--------------------------------------------------------------

	ArrayValue::ArrayValue()
		: ArrayValue(ArrayType())
	{}

	ArrayValue::ArrayValue(const ArrayType &value)
		: Value(ValueType::Array)
		, _value(value)
	{}

	ArrayType& ArrayValue::data()
	{
		return _value;
	}

	const ArrayType& ArrayValue::data() const
	{
		return _value;
	}

	bool ArrayValue::get_impl(const ArrayType*& out_value) const
	{
		out_value = &_value;
		return true;
	}

	bool ArrayValue::get_impl(ArrayType*& out_value)
	{
		out_value = &_value;
		return true;
	}

	//--------------------------------------------------------------

	//--------------------------------------------------------------
	ValuePtr Parser::parse(const std::string &str) const
	{
		if (str.empty())
		{
			return std::make_shared<NullValue>();
		}

		StringTypeConstItr it;
		return parse_value(str.begin(), str.end(), it);
	}

	ValuePtr Parser::parse_value(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		switch (*string_start)
		{
			case '[':
			{
				return parse_value_array(++string_start, string_end, token_end);
			}
			case '{':
			{
				return parse_value_object(++string_start, string_end, token_end);
			}
			case '\'':
			case '"':
			{
				return parse_value_string(string_start, string_end, token_end);
			}
			case 't':
			case 'f':
			case 'n':
			{
				return parse_value_bool(string_start, string_end, token_end);
			}
			default:
				return parse_value_number(string_start, string_end, token_end);
		}
	}

	ValuePtr Parser::parse_value_bool(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		auto token = parse_token(string_start, string_end, token_end);

		if (token == std::string("true"))
			return std::make_shared<BoolValue>(true);

		if (token == std::string("false"))
			return std::make_shared<BoolValue>(false);

		if (token == std::string("null"))
			return std::make_shared<NullValue>();

		return ValuePtr();
	}

	ValuePtr Parser::parse_value_number(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		// allowed: - . [0-9]
		auto parsing_float = false;
		auto negative = *string_start == '-';		
		auto it = string_start;
		if (negative)
			++it;

		for (; it < string_end; ++it)
		{
			if (!std::isdigit(*it))
			{
				if (*it == '.' && !parsing_float)
				{
					parsing_float = true;
					continue;
				}
				else if (*it == '.' && parsing_float)
				{
					//error!
					return ValuePtr();
				}

				token_end = it; token_end++;
				break;
			}
		}
		std::string value(string_start, it);
		if (parsing_float)
			return std::make_shared<FloatValue>(std::stof(value));

		return std::make_shared<IntValue>(std::stoi(value));
	}

	ValuePtr Parser::parse_value_string(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		auto quote_char = *string_start == '"' ? '"' : '\'';
		auto it = ++string_start;
		for (; it != string_end; ++it)
		{
			skip_ws(it, string_end, it);
			if (*it == quote_char)
			{
				token_end = it; token_end++;
				return std::make_shared<StringValue>(std::string(string_start, it));
			}
		}
		return ValuePtr();
	}

	ValuePtr Parser::parse_value_array(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		ArrayType arr;

		auto it = string_start;

		for (; it != string_end; ++it)
		{
			skip_ws(it, string_end, it);
			if (*it == ']')
			{
				++it;
				break;
			}
			else if (*it == ',')
			{
				continue;
			}
			arr.push_back(parse_value(it, string_end, it));
		}
		token_end = it;
		return std::make_shared<ArrayValue>(std::move(arr));
	}

	ValuePtr Parser::parse_value_object(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		ObjectType obj;
		auto it = string_start;
		for (; it != string_end; ++it)
		{
			skip_ws(it, string_end, it);
			if (*it == '}')
			{
				++it;
				break;
			}
			if (*it == ',')
			{
				continue;
			}
			auto name = parse_string(it, string_end, it);
			skip_ws(it, string_end, it);
			if (*it == ':')
			{
				skip_ws(++it, string_end, it);
				auto value = parse_value(it, string_end, it);
				obj.insert(std::make_pair(name, value));
			}			
		}
		token_end = it;
		return std::make_shared<ObjectValue>(obj);
	}

	void Parser::skip_ws(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		auto it = string_start;
		for (; it != string_end; ++it)
		{
			if (!std::isspace((*it)))
				break;
		}
		token_end = it;
	}

	std::string Parser::parse_token(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		auto it = string_start;
		for (; it != string_end; ++it)
		{
			if (!std::isalpha((*it)))
				break;
		}

		token_end = it;
		return std::string(string_start, token_end);
	}

	std::string Parser::parse_string(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const
	{
		auto quote_char = *string_start == '"' ? '"' : '\'';
		auto it = ++string_start;
		for (; it != string_end; ++it)
		{
			skip_ws(it, string_end, it);
			if (*it == quote_char)
			{
				token_end = it; token_end++;
				return std::string(string_start, it);
			}
		}
		return std::string();
	}

	//--------------------------------------------------------------

	void Writer::write(const ValuePtr& value, std::ostringstream& stream, const Formatting formatting)
	{
		switch (value->type())
		{
			case ValueType::Object:
			{
				write_object(std::move(value), stream, formatting);
				break;
			}
			case ValueType::Array:
			{
				write_array(std::move(value), stream, formatting);
				break;
			}
			case ValueType::Null:
			{
				stream << "null";
				break;
			}
			case ValueType::Bool:
			{
				bool bool_value;
				value->get(bool_value);
				stream << (bool_value ? "true" : "false");
				break;
			}
			case ValueType::Integer:
			{
				int int_value;
				value->get(int_value);
				stream << int_value;
				break;
			}
			case ValueType::Float:
			{
				float float_value;
				value->get(float_value);
				stream << float_value;
				break;
			}
			case ValueType::String:
			{
				std::string str_value;
				value->get(str_value);
				stream << "\"" << str_value << "\"";
				break;
			}
			default:
				break;
		}
	}

	void Writer::write_object(const ValuePtr& value, std::ostringstream& stream, const Formatting formatting)
	{
		const ObjectType* obj;
		value->get(obj);

		stream << "{";
		
		for (auto it = obj->begin(); it != obj->end();)
		{
			stream << "\"" << (*it).first << "\": ";
			write((*it).second, stream, formatting);
			if (++it != obj->end())
			{
				stream << ", ";
			}
		}

		stream << "}";
	}

	void Writer::write_array(const ValuePtr& value, std::ostringstream& stream, const Formatting formatting)
	{
		const ArrayType* arr;
		value->get(arr);
		stream << "[";
		for (auto it = arr->begin(); it != arr->end();)
		{
			write((*it), stream, formatting);
			if (++it != arr->end())
			{
				stream << ", ";
			}
		}
		stream << "]";
	}

	//--------------------------------------------------------------
	//--------------------------------------------------------------
	//--------------------------------------------------------------
}