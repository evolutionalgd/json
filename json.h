#pragma once

#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace json
{
	enum class ValueType
	{
		None,
		Null,
		Bool,
		Integer,
		Float,
		String,
		Object,
		Array
	};

	//--------------------------------------------------------------

	class Value;

	typedef std::shared_ptr<Value> ValuePtr;
	typedef std::vector<ValuePtr> ArrayType;
	typedef std::map<std::string, ValuePtr> ObjectType;

	class Value
	{
	public:
		Value();
		virtual ~Value() {}

		bool is_type(const ValueType type) const { return _type == type; }
		bool is_null() const { return _type == ValueType::None || _type == ValueType::Null; }

		ValueType type() const { return _type; }
		std::string type_name() const { return type_name_impl(); }

		float to_float() const;
		int to_int() const;
		bool to_bool() const;
		std::string to_string() const;
		const ArrayType* to_array() const;
		ArrayType* to_array();
		const ObjectType* to_object() const;
		ObjectType* to_object();

		bool get(float& out_value) const { return get_impl(out_value); }
		bool get(int& out_value) const { return get_impl(out_value); }
		bool get(bool& out_value) const { return get_impl(out_value); }
		bool get(std::string& out_value) const { return get_impl(out_value); }
		bool get(const ArrayType*& out_value) const { return get_impl(out_value); }
		bool get(ArrayType*& out_value) { return get_impl(out_value); }
		bool get(const ObjectType*& out_value) const { return get_impl(out_value); }
		bool get(ObjectType*& out_value) { return get_impl(out_value); }
	protected:
		explicit Value(const ValueType type);

		ValueType _type;

	private:
		virtual bool get_impl(float& out_value) const { return false; }
		virtual bool get_impl(int& out_value) const { return false; }
		virtual bool get_impl(bool& out_value) const { return false; }
		virtual bool get_impl(std::string& out_value) const { return false; }
		virtual bool get_impl(const ArrayType*& out_value) const { return false; }
		virtual bool get_impl(ArrayType*& out_value) { return false; }
		virtual bool get_impl(const ObjectType*& out_value) const { return false; }
		virtual bool get_impl(ObjectType*& out_value) { return false; }

		virtual std::string type_name_impl() const = 0;
	};

	//--------------------------------------------------------------

	class StringValue final
		: public Value
	{
	public:
		StringValue();
		explicit StringValue(const std::string& value);

		const std::string& data() const { return _value; }
		void data_set(const std::string& value) { _value = value; }

	protected:
		virtual bool get_impl(std::string& out_value) const override;

	private:
		virtual std::string type_name_impl() const override { return std::string("string"); }

		std::string _value;
	};

	//--------------------------------------------------------------

	class BoolValue final
		: public Value
	{
	public:
		BoolValue();
		explicit BoolValue(const bool value);

		bool data() const { return _value; }
		void data_set(const bool value) { _value = value; }

	protected:
		virtual bool get_impl(float& out_value) const override;
		virtual bool get_impl(int& out_value) const override;
		virtual bool get_impl(bool& out_value) const override;

	private:
		virtual std::string type_name_impl() const override { return std::string("boolean"); }

		bool _value;
	};

	//--------------------------------------------------------------

	class IntValue final
		: public Value
	{
	public:
		IntValue();
		explicit IntValue(const int value);

		int data() const { return _value; }
		void data_set(const int value) { _value = value; }

	protected:
		virtual bool get_impl(float& out_value) const override;
		virtual bool get_impl(int& out_value) const override;
		virtual bool get_impl(bool& out_value) const override;

	private:
		virtual std::string type_name_impl() const override { return std::string("integer"); }

		int _value;
	};

	//--------------------------------------------------------------

	class FloatValue final
		: public Value
	{
	public:
		FloatValue();
		explicit FloatValue(const float value);

		float data() const { return _value; }
		void data_set(const float value) { _value = value; }

	protected:
		virtual bool get_impl(float& out_value) const override;
		virtual bool get_impl(int& out_value) const override;
		virtual bool get_impl(bool& out_value) const override;

	private:
		virtual std::string type_name_impl() const override { return std::string("float"); }

		float _value;
	};

	//--------------------------------------------------------------

	class NullValue final
		: public Value
	{
	public:
		NullValue();

	protected:
		virtual bool get_impl(float& out_value) const override;
		virtual bool get_impl(int& out_value) const override;
		virtual bool get_impl(bool& out_value) const override;

	private:
		virtual std::string type_name_impl() const override { return std::string("null"); }
	};

	//--------------------------------------------------------------

	class ObjectValue final
		: public Value
	{
	public:
		ObjectValue();
		explicit ObjectValue(const ObjectType& value);

		ObjectType& data();
		const ObjectType& data() const;

	protected:
		virtual bool get_impl(const ObjectType*& out_value) const override;
		virtual bool get_impl(ObjectType*& out_value) override;

	private:
		virtual std::string type_name_impl() const override { return std::string("object"); }

		ObjectType _value;
	};

	//--------------------------------------------------------------

	class ArrayValue final
		: public Value
	{
	public:
		ArrayValue();
		explicit ArrayValue(const ArrayType &value);

		ArrayType& data();
		const ArrayType& data() const;

	protected:
		virtual bool get_impl(const ArrayType*& out_value) const override;
		virtual bool get_impl(ArrayType*& out_value) override;

	private:
		virtual std::string type_name_impl() const override { return std::string("array"); }

		ArrayType _value;
	};

	//--------------------------------------------------------------

	class Parser final
	{
	public:
		typedef std::string StringType;
		typedef StringType::const_iterator StringTypeConstItr;

		ValuePtr parse(const std::string &str) const;

	private:
		ValuePtr parse_value(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		ValuePtr parse_value_bool(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		ValuePtr parse_value_number(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		ValuePtr parse_value_string(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		ValuePtr parse_value_array(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		ValuePtr parse_value_object(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		void skip_ws(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		std::string parse_token(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
		std::string parse_string(StringTypeConstItr string_start, StringTypeConstItr string_end, StringTypeConstItr &token_end) const;
	};

	//--------------------------------------------------------------

	class Writer final
	{
	public:
		enum class Formatting
		{
			None,
		};

		void write(const ValuePtr& value, std::ostringstream& stream, const Formatting formatting = Formatting::None);

	private:
		void write_object(const ValuePtr& value, std::ostringstream& stream, const Formatting formatting);
		void write_array(const ValuePtr& value, std::ostringstream& stream, const Formatting formatting);
	};

}
